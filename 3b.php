class CAC_Cost extends Model
{
    public static function monthlyUsersCount()
    {
        $sql = 'select 
            count(o1.user_id) as aggregate 
            from 
            displaio.order o1
            LEFT JOIN displaio.order o2 ON o1.user_id = o2.user_id AND o2."payment_received_in_full" = true and o2."refunded" = false and o2.payment_receive_date::date < \'' .date("Y-m-01"). '\'
            where 
            o1."payment_received_in_full" = true 
            and o1."refunded" = false 
            and o1.payment_receive_date::date >= \'' .date("Y-m-01"). '\' 
            AND o2.user_id IS NULL';

        return \DB::connection('old-displate')->select($sql)[0]->aggregate;
    }

    public static function realCAC()
    {
    	$newCustomersCount = self::monthlyUsersCount();
        $cac = CAC::getCAC(date('Y-m-d'));
        $totalCost = self::whereRaw('create_date::date >= '. '\'' .date("Y-m-01"). '\'');
    	if ($newCustomersCount == 0) {
    		return "No new customers to calculate results";
    	}

        if (is_int($cac)) {
            return round(((self::getTotalCostSum($totalCost) / $newCustomersCount) / $cac) * 100,
                    2) . "% of target $" . $cac;
        }
        return "No CAC set up for current month";
    }
}