        {{-- CAC DISPLAY --}}
        <div class="col-lg-3 col-md-4">
            @if(intval(\App\CAC_Cost::realCAC()) > 95 || intval(\App\CAC_Cost::realCAC()) == 0)
            <div class="widget widget-danger widget-item-icon">
            @elseif(intval(\App\CAC_Cost::realCAC()) < 95)
            <div class="widget widget-success widget-item-icon">
            @else
            <div class="widget widget-warning widget-item-icon">
            @endif
                <div class="widget-item-left">
                    <span class="fa fa-usd fa-2x"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count" style="font-size: 22px;">${{ \App\CAC_Cost::cac() }}</div>
                    <div class="widget-title" style="font-size:14px;">CAC</div>
                    <div class="widget-subtitle"> {{ \App\CAC_Cost::realCAC() }}</div>
                </div>
                <div class="widget-controls">
                </div>
            </div>
        </div>