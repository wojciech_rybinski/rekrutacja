<?php

class Order extends Model implements UniqueHashId
{
    /*
     * Wyslij maila z faktura korygujaca
     */
    public function sendCorrectionInvoice()
    {
        $invoice_data = \App\Invoice::getCorrectionInvoice($this->id);
        $currency = Currency::find($this->currency_id);
        $formatter = $currency->getCurrencyFormatter();

        $html = view('admin.bookkeeping.invoice_correction_for_pdf')
            ->withData($invoice_data)
            ->withFormatter($formatter)
            ->withCurrency($currency)->render();


        $invoice_temp_path = storage_path('invoice_correction_pdfs') . '/' . str_replace('/', '_', $this->invoice_number) . '.pdf';
        /**
         * @var $pdfViewer InvoicePdfViewer
         */
        $pdfViewer = \App::make(InvoicePdfViewer::class);

        \Storage::disk('invoice_correction_pdfs')->put(str_replace('/', '_', $this->invoice_number) . '.pdf', $pdfViewer->showCorrection($this->id)->getContent());

        // wyslij maila
        $user = User::find($this->user_id);
        $content_html = view('templates::templates.standard')->with(['content' => view('emails.refund')->withEmail($user->email)->render()]);
        $subject = 'Order refunded.';
        \App\Mailer::sendSystemEmail(['potwierdzenia@displate.com' => 'Displate'], $content_html, $subject, 'noreply@displate.com', 'Displate', $invoice_temp_path);
        $mail_status = \App\Mailer::sendSystemEmail([$user->email => $this->address_name], $content_html, $subject, 'noreply@displate.com', 'Displate', $invoice_temp_path);
        if ($mail_status) {
            // usun fakture po jej wyslaniu
            unlink($invoice_temp_path);
            // update correction_invoice_send_date
            $this->correction_invoice_send_date = \Carbon\Carbon::now();
            $this->save();
        }

        $task = \Displate\Task\Model\Task::where('ref_id', $this->id)->where('task_type_id', 3)->where('done', false)->first();
        if (!empty($task))
            Task::closeTask($task->id);

        return ['Success' => $mail_status];
    }
}