<?php

declare(strict_types=1);


public function itemSize(): int
{
    $result = 'zero';

    foreach ($items as $item) {
        if ($item->id() === 55) {
            $result = $item->size();
        }
    }

    return $result;
}
